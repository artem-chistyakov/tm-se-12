package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOne(@NotNull String id) throws SQLException;

    boolean remove(@NotNull String id) throws SQLException;

    boolean persist(@NotNull User user) throws SQLException;

    boolean merge(@NotNull User user) throws SQLException;

    boolean update(@NotNull User user) throws SQLException;

    User findOneWithLoginPassword(@Nullable  String login,@Nullable  String password) throws SQLException;

    @Nullable
    boolean insert(@Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws SQLException;

    @NotNull
    Collection<User> findAll() throws SQLException;
}
