package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;

import java.sql.SQLException;

public interface ISessionRepository {

    Session create(@Nullable Session session) throws SQLException;

    boolean contains(@Nullable String sessionId) throws SQLException;

    boolean delete(@Nullable String sessionId) throws SQLException;
}
