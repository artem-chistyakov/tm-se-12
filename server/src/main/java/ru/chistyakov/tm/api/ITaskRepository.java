package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface ITaskRepository extends IRepository<Task> {

     boolean insert(@NotNull String userId, @NotNull String projectId, @NotNull String taskName,
                @Nullable String descriptionTask, @Nullable Date dateBeginTask, @Nullable Date dateEndTask) throws SQLException;

    boolean persist(@NotNull Task task) throws SQLException;

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String taskId) throws SQLException;

    boolean update(@NotNull Task task) throws SQLException;

    @NotNull
    Collection<Task> findAllInOrder(@NotNull String idUser, @NotNull String part) throws SQLException;

    boolean remove(@NotNull String userId, @NotNull String taskId) throws SQLException;

    boolean removeByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;

    void removeAll(@NotNull String userId) throws SQLException;

    @Nullable
    Collection<Task> findAll(@NotNull String userId) throws SQLException;

    boolean merge(@NotNull Task task) throws SQLException;

    @Nullable
    Collection<Task> findByPartNameOrDescription(String idUser, String part) throws SQLException;

    Collection<Task> findAll() throws SQLException;
}
