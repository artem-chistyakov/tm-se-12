package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {


    boolean insert(@NotNull String userId, @NotNull String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd) throws SQLException;

    @Nullable
    boolean persist(@NotNull Project project) throws SQLException;

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String projectId) throws SQLException;

    @NotNull
    Collection<Project> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    Collection<Project> findAllInOrder(@NotNull String userId, String str) throws SQLException;

    boolean update(@NotNull Project project) throws SQLException;

    boolean remove(@NotNull String userId, @NotNull String projectId) throws SQLException;

    void removeAll(@NotNull String userId) throws SQLException;

    boolean merge(@NotNull Project project) throws SQLException;

    @NotNull
    Collection<Project> findByPartNameOrDescription(String userId, String part) throws SQLException;

    @NotNull
    Collection<Project> findAll() throws SQLException;
}
