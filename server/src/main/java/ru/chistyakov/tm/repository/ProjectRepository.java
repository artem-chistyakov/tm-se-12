package ru.chistyakov.tm.repository;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.FieldConst;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    final private Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull Collection<Project> findAll() throws SQLException{
        final Collection<Project> collection = new ArrayList<>();
        final String query = "select * from app_project;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }

    @Override
    @Nullable
    public boolean insert(@NotNull final String userId, @NotNull final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) throws SQLException {
        final String query = "insert into task_manager_bd.app_project(id,date_begin,date_end,description,name,user_id) values (?,?,?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, UUID.randomUUID().toString());
        if (dateBegin != null) preparedStatement.setDate(2, new java.sql.Date(dateBegin.getTime()));
        else preparedStatement.setDate(2, null);
        if (dateEnd != null) preparedStatement.setDate(3, new java.sql.Date(dateEnd.getTime()));
        else preparedStatement.setDate(3, null);
        preparedStatement.setString(4, description);
        preparedStatement.setString(5, name);
        preparedStatement.setString(6, userId);
        preparedStatement.execute();
        return true;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        final String query = "select * from app_project where user_id = ? and id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) return fetch(resultSet);
        return null;
    }

    @Override
    @NotNull
    public Collection<Project> findAllInOrder(@NotNull final String userId, @NotNull final String order) throws SQLException {
        final Collection<Project> projectCollection = new ArrayList<>();
        final String query = "select * from app_project where user_id = ? order by ? asc;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, order);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) projectCollection.add(fetch(resultSet));
        return projectCollection;
    }

    @Override
    @NotNull
    public Collection<Project> findAll(@NotNull final String userId) throws SQLException {
        final Collection<Project> projectCollection = new ArrayList<>();
        final String query = "select * from app_project where user_id = ? ;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) projectCollection.add(fetch(resultSet));
        return projectCollection;
    }

    @Override
    @Nullable
    public boolean update(@NotNull final Project project) throws SQLException {
        final String query = "update app_project set name = ?,description = ?,date_begin = ?,date_end = ?, where id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setDate(3, new java.sql.Date(project.getDateBeginProject().getTime()));
        preparedStatement.setDate(4, new java.sql.Date(project.getDateEndProject().getTime()));
        preparedStatement.setString(5, project.getId());
        preparedStatement.execute();
        return true;
    }

    @Override
    @Nullable
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        final String query = "delete from app_project where user_id = ? and id = ? ;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.execute();
        return true;
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        final String query = "delete from app_project where user_id = ?; ";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeQuery();
    }

    @Override
    public boolean persist(@NotNull final Project project) throws SQLException {
        final String query = "insert into app_project values (?,?,?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        if (project.getDateBeginProject() != null)
            preparedStatement.setDate(4, new java.sql.Date(project.getDateBeginProject().getTime()));
        if (project.getDateEndProject() != null)
            preparedStatement.setDate(5, new java.sql.Date(project.getDateEndProject().getTime()));
        preparedStatement.setString(6, project.getUserId());
        preparedStatement.execute();
        return true;
    }

    @Override
    public boolean merge(@NotNull final Project project) throws SQLException {
        if (findOne(project.getUserId(), project.getId()) == null) return persist(project);
        else
            return update(project);
    }

    @Override
    @NotNull
    public Collection<Project> findByPartNameOrDescription(@NotNull final String userId, @NotNull final String part) throws SQLException {
        final Collection<Project> projectCollection = new ArrayList<>();
        final String query = "select * from app_project where user_id = ? and name like ? or description like ?; ";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, part);
        statement.setString(2, part);
        final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) projectCollection.add(fetch(resultSet));
        return projectCollection;
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID.name()));
        project.setName(row.getString(FieldConst.NAME.name()));
        project.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
        project.setDateBeginProject(row.getDate(FieldConst.DATE_BEGIN.name()));
        project.setDateEndProject(row.getDate(FieldConst.DATE_END.name()));
        project.setUserId(row.getString(FieldConst.USER_ID.name()));
        return project;
    }
}
