package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.FieldConst;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@XmlRootElement
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    final private Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insert(@Nullable final String login, @Nullable final String password, @Nullable RoleType roleType) throws SQLException {
        final String query = "insert into task_manager_bd.app_user(id,login,password_hash,role) values(?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, UUID.randomUUID().toString());
        preparedStatement.setString(2, login);
        preparedStatement.setString(3, password);
        preparedStatement.setString(4, roleType.toString());
        preparedStatement.execute();
        return true;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) throws SQLException {
        final String query = "select * from app_user where id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) return fetch(resultSet);
        return null;
    }

    @Override
    public boolean remove(@Nullable final String id) throws SQLException {
        final String query = "delete from app_user where id = ?";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        return true;

    }

    @Override
    public boolean persist(@Nullable final User user) throws SQLException {
        final String query = "insert into app_user values(?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(1, user.getPassword());
        preparedStatement.setString(1, user.getRoleType().toString());
        preparedStatement.execute();
        return true;

    }

    @Override
    public boolean merge(@Nullable final User user) throws SQLException {
        if (user == null) return false;
        if (findOne(user.getId()) == null) return persist(user);
        else
            return update(user);
    }


    @Override
    public boolean update(@Nullable final User user) throws SQLException {

        final String query = "update app_user set login = ?,password_hash =?,role =?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getRoleType().toString());
        preparedStatement.execute();
        return true;

    }

    @Override
    @Nullable
    public User findOneWithLoginPassword(@Nullable final String login, @Nullable final String password) throws SQLException {
        final String query = "select * from app_user where login =? and password_hash =?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) return fetch(resultSet);
        return null;
    }

    @Override
    public @NotNull Collection<User> findAll() throws SQLException {
        final Collection<User> collection = new ArrayList<>();
        final String query = "select * from app_user;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) {
        try {
            if (row == null) return null;
            @NotNull final User user = new User();
            user.setId(row.getString(FieldConst.ID.name()));
            user.setLogin(row.getString(FieldConst.LOGIN.name()));
            user.setPassword(row.getString(FieldConst.PASSWORD_HASH.name()));
            user.setRoleType(RoleType.valueOf(row.getString(FieldConst.ROLE.name())));
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}