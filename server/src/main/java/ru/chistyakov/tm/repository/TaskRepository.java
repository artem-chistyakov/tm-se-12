package ru.chistyakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.FieldConst;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    @NotNull
    final private Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insert(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName,
                          @Nullable final String descriptionTask, @Nullable final Date dateBeginTask,
                          @Nullable final Date dateEndTask) throws SQLException {
        final String query = "insert into app_task values (?,?,?,?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, UUID.randomUUID().toString());
        preparedStatement.setString(2, taskName);
        preparedStatement.setString(3, descriptionTask);
        if (dateBeginTask != null) preparedStatement.setDate(4, new java.sql.Date(dateBeginTask.getTime()));
        if (dateEndTask != null) preparedStatement.setDate(5, new java.sql.Date(dateEndTask.getTime()));
        preparedStatement.setString(6, userId);
        preparedStatement.setString(7, projectId);
        preparedStatement.execute();
        return true;

    }

    @Override
    public boolean persist(@Nullable final Task task) throws SQLException {
        final String query = "insert into app_task values (?,?,?,?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        if (task.getDateBeginTask() != null)
            preparedStatement.setDate(4, new java.sql.Date(task.getDateBeginTask().getTime()));
        if (task.getDateEndTask() != null)
            preparedStatement.setDate(5, new java.sql.Date(task.getDateEndTask().getTime()));
        preparedStatement.setString(6, task.getUserId());
        preparedStatement.setString(7, task.getProjectId());
        preparedStatement.execute();
        return true;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String userId, @Nullable final String idTask) throws SQLException {
        final String query = "select * from app_task where user_id = ?  and id =?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, idTask);
        final ResultSet resultSet = preparedStatement.executeQuery();
        return fetch(resultSet);
    }

    @Override
    public boolean update(@NotNull Task task) throws SQLException {
        final String query = "update app_task set  name = ?,description = ?,date_begin = ?,date_end = ?, where id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, task.getName());
        preparedStatement.setString(2, task.getDescription());
        if (task.getDateBeginTask() != null)
            preparedStatement.setDate(3, new java.sql.Date(task.getDateBeginTask().getTime()));
        if (task.getDateEndTask() != null)
            preparedStatement.setDate(4, new java.sql.Date(task.getDateEndTask().getTime()));
        preparedStatement.setString(5, task.getId());
        preparedStatement.execute();
        return true;
    }

    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String taskId) throws SQLException {
        if (userId == null || taskId == null) return false;
        final String query = "delete from app_task where user_id = ? and id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, taskId);
        preparedStatement.execute();
        return true;
    }

    @Override
    public boolean removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        final String query = "delete from app_task where user_id = ? and project_id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.execute();
        return true;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        final String query = "delete from app_task where user_id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null) return null;
        final Collection<Task> collection = new ArrayList<>();
        final String query = "select * from app_task where user_id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }

    @Override
    public @NotNull Collection<Task> findAll() throws SQLException {
        final Collection<Task> collection = new ArrayList<>();
        final String query = "select * from app_task;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }

    @Override
    public @Nullable boolean merge(@Nullable final Task task) throws SQLException {
        if (findOne(task.getUserId(), task.getId()) == null) return persist(task);
        else
            return update(task);
    }

    @Override
    public @NotNull Collection<Task> findAllInOrder(@Nullable final String userId, @Nullable final String order) throws SQLException {
        final String query = "Select * from app_task where user_id = ? order by ? asc;";
        final Collection<Task> collection = new ArrayList<>();
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, order);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }


    @Override
    public @Nullable Collection<Task> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) throws SQLException {
        final String query = "Select * from app_task where user_id = ? and name like ? and description like ?";
        final Collection<Task> collection = new ArrayList<>();
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, part);
        preparedStatement.setString(3, part);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) collection.add(fetch(resultSet));
        return collection;
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID.name()));
        task.setName(row.getString(FieldConst.NAME.name()));
        task.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
        task.setDateBeginTask(row.getDate(FieldConst.DATE_BEGIN.name()));
        task.setDateEndTask(row.getDate(FieldConst.DATE_END.name()));
        task.setUserId(row.getString(FieldConst.USER_ID.name()));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID.name()));
        return task;
    }
}

