package ru.chistyakov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionRepository;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.FieldConst;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.service.PropertiesService;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    final private Connection connection;

    @NotNull
    private final PropertiesService propertiesService;

    public SessionRepository(@NotNull final PropertiesService propertiesService, @NotNull final Connection connection) {
        this.propertiesService = propertiesService;
        this.connection = connection;
    }

    @Override
    public Session create(@Nullable final Session session) throws SQLException {
        final String query = "insert into app_session values(?,?,?,?,?);";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getSignature());
        preparedStatement.setBigDecimal(3, new BigDecimal(session.getTimestamp().toString()));
        preparedStatement.setString(4, session.getRoleType().toString());
        preparedStatement.setString(5, session.getUserId());
        preparedStatement.execute();
        final String query2 = "Select * from app_session where id = ?;";
        final PreparedStatement preparedStatement2 = connection.prepareStatement(query2);
        preparedStatement2.setString(1, session.getId());
        final ResultSet resultSet = preparedStatement2.executeQuery();
        while (resultSet.next()) return fetch(resultSet);
        return null;
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws SQLException {
        final String query = "select * from app_session where id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        preparedStatement.execute();
        return true;
    }

    @Override
    public boolean delete(@Nullable final String sessionId) throws SQLException {
        final String query = "delete from app_session where id = ?;";
        final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        preparedStatement.execute();
        return true;

    }

    @Nullable
    @SneakyThrows
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID.name()));
        session.setSignature(row.getString(FieldConst.SIGNATURE.name()));
        session.setTimestamp(Long.parseLong(row.getBigDecimal(FieldConst.TIMESTAMP.name()).toString()));
        session.setRoleType(RoleType.valueOf(row.getString(FieldConst.ROLE.name())));
        session.setUserId(row.getString(FieldConst.USER_ID.name()));
        return session;
    }
}
