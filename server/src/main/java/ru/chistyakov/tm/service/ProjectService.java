package ru.chistyakov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.FieldConst;
import ru.chistyakov.tm.repository.ProjectRepository;
import ru.chistyakov.tm.repository.TaskRepository;
import ru.chistyakov.tm.utility.ConnectionUtility;
import ru.chistyakov.tm.utility.DateParser;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.merge(project);
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.persist(project);
        } catch (SQLException exception) {
            exception.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String descriptionProject, @Nullable final String dateBeginProject,
                          @Nullable final String dateEndProject) {
        try (final Connection connection = ConnectionUtility.getConnection()) {
            if (userId == null || name == null) return false;
            if (name.trim().isEmpty() || userId.trim().isEmpty()) return false;
            final ProjectRepository projectRepository = new ProjectRepository(connection);
            return projectRepository.insert(userId, name, descriptionProject,
                    DateParser.parseDate(simpleDateFormat, dateBeginProject),
                    DateParser.parseDate(simpleDateFormat, dateEndProject));
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.trim().isEmpty()) return;
        try (final Connection projectConnection = ConnectionUtility.getConnection();
             final Connection taskConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            final TaskRepository taskRepository = new TaskRepository(taskConnection);
            projectRepository.removeAll(userId);
            taskRepository.removeAll(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return false;
        if (projectId.trim().isEmpty()) return false;
        try (final Connection projectConnection = ConnectionUtility.getConnection();
             final Connection taskConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            final TaskRepository taskRepository = new TaskRepository(taskConnection);
            taskRepository.removeByProjectId(userId, projectId);
            return projectRepository.remove(userId, projectId);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findOne(projectId, userId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(@Nullable final String projectId, @Nullable final String projectName,
                          @Nullable final String descriptionProject, @Nullable final String dateBegin,
                          @Nullable final String dateEnd, @Nullable final String userId) {
        final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(descriptionProject);
        if (dateBegin != null) project.setDateBeginProject(DateParser.parseDate(simpleDateFormat, dateBegin));
        if (dateEnd != null) project.setDateEndProject(DateParser.parseDate(simpleDateFormat, dateEnd));
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.update(project);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findAll(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findAllInOrder(userId, FieldConst.DATE_BEGIN.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findAllInOrder(userId, FieldConst.DATE_END.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findAllInOrder(userId, FieldConst.ROLE.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Collection<Project> findByPartNameOrDescription(@Nullable final String idUser, @Nullable final String part) {
        if (idUser == null || part == null) return null;
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findByPartNameOrDescription(idUser, part);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public Collection<Project> findAll() {
        try (final Connection projectConnection = ConnectionUtility.getConnection()) {
            final ProjectRepository projectRepository = new ProjectRepository(projectConnection);
            return projectRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}