package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.FieldConst;
import ru.chistyakov.tm.repository.TaskRepository;
import ru.chistyakov.tm.utility.ConnectionUtility;
import ru.chistyakov.tm.utility.DateParser;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;


public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    public boolean merge(@Nullable final Task task) {
        if (task == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.merge(task);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean persist(@Nullable final Task task) {
        if (task == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.persist(task);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public  boolean insert(@Nullable final String userId, @Nullable final String projectId,
                       @Nullable final String taskName, @Nullable final String descriptionTask,
                       @Nullable final String dateBeginTask, @Nullable final String dateEndTask) {
        if (userId == null || projectId == null || taskName == null) return false;
        if (userId.trim().isEmpty()) return false;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.insert(
                    userId, projectId, taskName, descriptionTask,
                    DateParser.parseDate(simpleDateFormat, dateBeginTask),
                    DateParser.parseDate(simpleDateFormat, dateEndTask));
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return null;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findOne(userId, taskId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAll(userId);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean update(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId,
                       @Nullable final String taskName, @Nullable final String description, @Nullable final String dateBegin,
                       @Nullable final String dateEnd) {
        if (userId == null || taskId == null || projectId == null || taskName == null) return false;
        if (taskId.trim().isEmpty() || taskName.trim().isEmpty()) return false;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            final Task task = new Task();
            task.setId(taskId);
            task.setProjectId(projectId);
            task.setDescription(description);
            task.setDateBeginTask(DateParser.parseDate(simpleDateFormat,dateBegin));
            task.setDateEndTask(DateParser.parseDate(simpleDateFormat,dateEnd));
            task.setUserId(userId);
            task.setName(taskName);
            return taskRepository.update(task);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || taskId == null) return false;
        if (userId.trim().isEmpty() || taskId.trim().isEmpty()) return false;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.remove(userId, taskId);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAll(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllInOrder(userId, FieldConst.DATE_BEGIN.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllInOrder(userId, FieldConst.DATE_END.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public @Nullable Collection<Task> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllInOrder(userId, FieldConst.ROLE.name());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public @Nullable Collection<Task> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findByPartNameOrDescription(userId, part);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @NotNull
    public Collection<Task> findAll() {
        try (final Connection connection = ConnectionUtility.getConnection()
        ) {
            final TaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
