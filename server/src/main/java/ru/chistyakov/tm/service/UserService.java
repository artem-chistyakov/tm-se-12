package ru.chistyakov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.UserRepository;
import ru.chistyakov.tm.utility.ConnectionUtility;
import ru.chistyakov.tm.utility.PasswordParser;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.persist(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findOneWithLoginPassword(login, passwordMD5);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean updateUser(@Nullable final String userId, @Nullable final String login, @Nullable final String password) {
        if (userId == null || login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final User user = findById(userId);
            if (user == null) return false;
            user.setLogin(login);
            user.setPassword(password);
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updatePassword(@Nullable final String userId, @Nullable final String login, @Nullable final String password) {
        if (userId == null || login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final User user = findById(userId);
            if (user == null) return false;
            user.setPassword(password);
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public User findWithLoginPassword(@Nullable String login, @Nullable String password) {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findOneWithLoginPassword(login, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public boolean registryAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.insert(login, passwordMD5, RoleType.ADMINISTRATOR);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean registryUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        if(findWithLoginPassword(login,passwordMD5)!= null) return false;
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.insert(login, passwordMD5, RoleType.USUAL_USER);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    @Nullable
    public User findById(String userId) {
        try (final Connection connection =  ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findOne(userId);
        } catch (
                SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Nullable
    public Collection<User> findAll() {
        try (final Connection connection = ConnectionUtility.getConnection()) {
            final UserRepository userRepository = new UserRepository(connection);
            return userRepository.findAll();
        } catch (
                SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
