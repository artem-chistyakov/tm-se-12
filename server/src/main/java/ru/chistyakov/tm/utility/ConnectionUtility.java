package ru.chistyakov.tm.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtility {
    public static Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            final Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/task_manager_bd",
                    "root", "root");
            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}