package ru.chistyakov.tm.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class SignatureUtil {

    public static String sign(
            @Nullable final  Object value,
            @Nullable final  String salt,
            @Nullable final Integer cycle
    ){
        try{
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final  String json = objectMapper.writeValueAsString(value);
            return sign(json,salt,cycle);
        }catch (final JsonProcessingException exception){
            return null;
        }
    }

    @Nullable
    private static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (value == null || salt == null || cycle == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = PasswordParser.parse(salt + result + salt);
        }
        return result;
    }

}
