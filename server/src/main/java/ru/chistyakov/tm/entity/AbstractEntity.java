package ru.chistyakov.tm.entity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;


public abstract class AbstractEntity implements Serializable {

    @NotNull
    String id;

    AbstractEntity() {
        id = UUID.randomUUID().toString();
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
