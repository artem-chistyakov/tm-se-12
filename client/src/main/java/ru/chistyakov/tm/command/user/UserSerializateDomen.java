package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;

public class UserSerializateDomen extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usd";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сериализация доменной области";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getUserEndpoint().serializateDomain(serviceLocator.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
