package ru.chistyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выход из программы";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null, RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
