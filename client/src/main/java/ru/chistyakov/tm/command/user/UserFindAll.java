package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.User;
import ru.chistyakov.tm.command.AbstractCommand;

public class UserFindAll extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "ufa";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все профили пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (final User user : serviceLocator.getUserEndpoint().findAllUser(serviceLocator.getSession()))
            System.out.println("User{" +
                    "login='" + user.getLogin() + '\'' +
                    ", password='" + user.getPassword() + '\'' +
                    ", roleType=" + user.getRoleType() +
                    ", id='" + user.getId() + '\'' +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
