package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.Session;
import ru.chistyakov.tm.command.AbstractCommand;

public final class UserAuthorizationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

    @Override
    public void execute() {
        System.out.println("введите имя");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("введите пароль");
        final String password = serviceLocator.getScanner().nextLine();
        final Session session = serviceLocator.getUserEndpoint().authorizeUser(login, password);
        if (session == null) System.out.println("Пользователя с таким логином и паролем не существует");
        else serviceLocator.setSession(session);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null};
    }
}
