package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class TaskRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "trac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление всех команд авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getTaskEndpoint().removeAllTask(serviceLocator.getSession());
        System.out.println("Все задачи удалены");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
