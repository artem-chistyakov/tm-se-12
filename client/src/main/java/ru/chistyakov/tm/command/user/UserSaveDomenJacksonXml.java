package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

import java.io.IOException;

public class UserSaveDomenJacksonXml extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usdjx";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сохранение предметной области в формате xml с помощью fasterxml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException{
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getUserEndpoint().saveDomainJacksonXml(serviceLocator.getSession());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
