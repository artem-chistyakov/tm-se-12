package ru.chistyakov.tm.api;

import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    IProjectEndpoint getProjectEndpoint();

    Map<String, AbstractCommand> getCommandMap();

    IUserEndpoint getUserEndpoint();

    ITaskEndpoint getTaskEndpoint();

    ISessionEndpoint getSessionEndpoint();

    void setSession(Session session);

    Session getSession();

    Scanner getScanner();
}
